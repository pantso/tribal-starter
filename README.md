# Tribal Front-end Starter project (WIP)

The Tribal Worldwide Athens Front-end starter project, for website and web app development. 
Based on:

* **Webpack** (latest build)
* **Custom Bootstrap 3/4** (you can select accordingly)
* **SASS**
* **Normalize.css**
* **Animate.css**
* **Fontawesome.css** (you can switch between font systems)
* **Fontastic.me** (you can switch between font systems)

Also includes some JS plugins to be activated as wished:

* **jQuery** (included by default)
* **Flickity Slider** (not included by default)
* **Fancybox 3** (not included by default)
* **JCF** (not included by default)
* **TweenMax** (not included by default)
* **Anime.js** (not included by default)

All inclusions are controlled by commented out code in the `webpack.config.js`

## Getting Started

First, clone the project:

```
bash
$ git clone https://pantso@bitbucket.org/pantso/tribal-starter.git <your-project-name>
$ cd <your-project-name>
```

then run

```
dsadsadsa
```

and 

```
dsadsadsa
```

to install all dependencies

Make sure you have the latest `npm` version.